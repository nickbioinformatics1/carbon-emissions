# Carbon Emissions

## Summary
This is a POC concept repo to download weather data using [openweathermap](www.openweathermap.org) api and visualize the data using dash. 

## How it works
Using a cron job, a dockerized version of `loadData.py` script queries the [Open Weather Map API](openweathermap.org/api) using api url in `config.py`. Once the data has been queried, the data is wrangled using `pandas` and inserted into a `postgres` database. Additionally, a simple dashboard `app.py` was created to view the data.

## Dependencies
1. Docker
2. python3: See rquirements.txt for dependencies
3. Postgres database
4. System able to run cronjobs. This example was developed on a debian system.
5. [Open Weather Map API](openweathermap.org/api) access

## Setting up postgres Database
The database was hosted on a raspiberry pi model 3. The following commands were used to set up the postgres database: 
1. sudo apt install postgresql
2. sudo su postgres
3. createuser apple
4. For password: <enter password>
5. Launch psql terminal  

```psql
-- Create Database
CREATE DATABASE test;

-- Create table with specific fields
CREATE TABLE weathcron(
    temperature float(4) NOT NULL, 
    feelslike float(4) NOT NULL, 
    pressure float(4) NOT NULL,
    humidity float(4) NOT NULL,
    city VARCHAR(255), 
    date_taken DATE NOT NULL, 
    timestamp TIMESTAMP NOT NULL
);
```

## Building docker container
To build the docker container, run:  
```bash
# In the directory containing Dockerfile
docker build -t carbonemissions .
```
## Setting up Cronjob
```bash
# Open crontab
crontab -e

# Add following job for each city. This will run every hour
# Weather data for chicago
0 * * * * docker run carbonemissions -c Chicago
```

To start the cron service, run
```bash
# Developed on debian
sudo service cron start
```


