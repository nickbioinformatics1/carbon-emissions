import config
import requests
import pandas as pd
import argparse
import sys
import psycopg2 as pg
from datetime import date
from datetime import datetime

# Queries the api for a specific city, wrangles data into pandas.dataframe, formats date and timestamp
def getWeatherData(city):
    api_url= config.api_token + city
    jsonData=requests.get(api_url).json()
    weatherData=jsonData['main']
    df = pd.DataFrame.from_dict(weatherData,orient='index').transpose()
    df['city']=city
    df['datestamp']=date.today()
    df['timestamp']=datetime.now().strftime('%H:%M:%S')
    return(df)

# Checks connection to postgres database
def connectdb():
    conn=None
    try:
        conn=pg.connect(host=config.host,port=config.dbport,user=config.dbuser, password=config.dbpsswd,database=config.dbname)
        print("Sucessfully connected to database")
    except(Exception, pg.Error) as error:
        print("There was an error connecting to database",error)
    return(conn)

# Inserts data into postgres database
def insertData(conn,query):
    cursor=conn.cursor()
    try:
        cursor.execute(query)
        conn.commit()
    except (Exception,pg.DatabaseError) as error:
        print("Error: %s" %error)
        conn.rollback()
        cursor.close()
        sys.exit(1)
    cursor.close()

# Main program, Add arguments, performs postgres query to insert data into database, closes database connection
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c","--city",required=True,help="Weather Data City")
    parser.add_argument("-t","--test",action="store_true",help="tests code until upload point")
    args = parser.parse_args()

    weathData=getWeatherData(args.city)
    print(weathData)
    connection=connectdb()
    for index, row in weathData.iterrows():
        query= """
        INSERT into weathcron(temperature, feelslike, pressure, humidity, city, date_taken, timestamp) values(%s,%s,%s,%s,'%s','%s','%s');
        """ % (row['temp'],row['feels_like'],row['humidity'],row['pressure'],row['city'],row['datestamp'],row['timestamp'])
        insertData(connection,query)
    connection.close()