import dash
from dash.dependencies import Output, Input
import dash_core_components as dcc
import dash_html_components as html
import ploty as plt
import ploty.graph_objs as go
import config

app = dash.Dash(__name__)

app.layout = html.Div(
    [
        dcc.Graph(id='livePlot',animiate=True),
        dcc.Interval(id='updatePlot',interval=1000,n_intervals=0),
    ]
)

@app.callback(
    Output('livePlot','figure'),
    [Input('updatePlot','n_intervals')]
)



