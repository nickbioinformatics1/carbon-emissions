From python:3.8

WORKDIR requests

COPY requirements.txt .
COPY loadData.py .
COPY config.py .
RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT ["python3", "./loadData.py"]